#!/usr/bin/env ruby
#######################################
#                                     #
# ruby 1.8.7                          #
# 2018, REPAIN Paul                   #
# GitLab : @Poulposaure               #
# GitHub : @Poulpy                    #
#                                     #
#                                     #
#                                     #
#######################################
# __  __ _                 _          #
#|  \/  (_)_____ __ ___ __(_)___ _ _  #
#| |\/| | / -_) V  V / '_ \ / _ \ ' \ #
#|_|  |_|_\___|\_/\_/| .__/_\___/_||_|#
#                    |_|              #
#######################################

####################
#                  #
#    CONSTANTES    #
#   ET VARIABLES   #
#     GLOBALES     #
#                  #
####################

$n = 3# cote du morpion
$limite = $n*$n
$tempsLimite = 7# temps limite pour pouvoir entrer une reponse
$tempsLimitePartie = 30# temps limite d'une partie
$VIDE = ' '
$CERCLE = 'O'
$CROIX = 'X'
$SYMBOLES = [$CROIX, $CERCLE]

####################
#                  #
#    STRUCTURES    #
#                  #
####################

class Joueur
  attr_accessor :humain, :symbole

  ###############
  #CONSTRUCTEURS#
  ###############

  def initialize(symbole)
    @symbole = symbole
    @humain = true
  end

  def initialize(symbole, bool)
    @symbole = symbole
    @humain = bool
  end

  ##############
  #  METHODES  #
  ##############

  def to_s
    "#@symbole, #@humain"
  end
end# Joueur

class Partie
  attr_accessor :plateau, :nbTours

  ##############
  #CONSTRUCTEUR#
  ##############

  def initialize
    @plateau = Array.new($n) { Array.new($n, $VIDE) }# le morpion (tableau 2D)
    @nbTours = 0
  end# initialize

  ##############
  #  METHODES  #
  ##############

  def to_s
    "#@plateau, @nbTours"
  end

  def caseVide?(ligne, colonne)
    return @plateau[ligne-1][colonne[0]-97].eql?($VIDE)
  end# caseVide

  def victoire?(symbole)
    """Verifie si le joueur a gagne. Regarde la diagonale
    haut-gauche=>bas-droite puis l'autre diagonale (simples
    boucles) puis regarde les lignes et les colonnes
	(doubles boucles)"""
    lig = 0
    col=0
    while col != $n and lig != $n and @plateau[col][lig] == symbole
      col += 1
      lig += 1
    end
    
    return true if col==$n and lig==$n# si on est arrive jusqu'au bout
    
    lig = 0
    col = $n-1
    while col != -1 and lig != $n and @plateau[lig][col] == symbole
      col -= 1
      lig += 1
    end
    
    return true if col == -1 and lig == $n
    
    for i in (0..$n-1)
      lig = 0
      lig += 1 while lig != $n and @plateau[lig][i] == symbole

      
      col=0
      
      col += 1 while col != $n and @plateau[i][col] == symbole
      
      
      return true if col == $n or lig == $n
      
    end
    return false
  end# victoire
end# Partie

###################
#                 #
#    FONCTIONS    #
#                 #
###################

def main
  while true
    system "clear" or system "cls"# efface la console
    puts "  __  __ _                 _          "
    puts " |  \\/  (_)_____ __ ___ __(_)___ _ _  "
    puts " | |\\/| | / -_) V  V / '_ \\ / _ \\ ' \\ "
    puts " |_|  |_|_\\___|\\_/\\_/| .__/_\\___/_||_|"
    puts "                     |_|              ✧/ᐠ-ꞈ-ᐟ\\\n\n"
    puts " - - - - - - - - - - - - - - - - - - - - - - -\n\n"
    puts "(1) Joueur VS Joueur"
    puts "(2) Joueur VS Ordinateur"
    puts "(3) Configurations"
    print "(0) Quitter\n\n>>> "
    saisie = gets.chomp.to_i
    case saisie
    when 1
      joue(2)
    when 2
      joue(1)
    when 3
      configurations
    when 0
      puts "Merci d'avoir joué à Miewpion ! (=´ᆺ｀=)"
      return
    end# case
  end# while
end# main

def joue(humain)
  # initialisation des joueurs
  joueurs = []*2
  for symbole in 0..1
    joueur = Joueur.new($SYMBOLES[symbole], humain != symbole)
    joueurs[symbole] = joueur
  end
  partie, ordre, debut = initialisePartie(humain == 1)

  while true
    for joueur in ordre
      afficheMorpion(partie.plateau)
      print "Tour du joueur ", joueur, "\n"
      statut = joueTour(joueurs[joueur], partie)
      statut = 3 if Time.now - debut >= $tempsLimitePartie# Egalite si le temps limite est ecoule
      case statut
      when 1
        print "Joueur ", joueur, " a gagne ! (≈ㅇᆽㅇ≈)♡\n"
      when 3
        puts "Temps ecoule !" if Time.now-debut >= $tempsLimitePartie
        puts 'Egalite... (=✖ᆽ✖=)'
      end# case
      if statut == 1 or statut == 3
        print "Jeu fini en ", (Time.now-debut).to_i, " secondes\n\n"
        puts "Quitter (O/o/N/n) ? "
        saisie = gets.chomp
        case saisie
        when 'O', 'o'# Quitter
          return
        when 'n', 'N'# Rejouer
          partie, ordre, debut = initialisePartie(humain == 1)
          next
        end# case
      end# if
    end# for
  end# while
end# joue


def configurations
  system "clear" or system "cls"# efface la console
  puts "CONFIGURATIONS\n"
  puts "- - - - - - - - - - - - - - - - - - - - - - - -\n"
  puts "(1) Modifier taille du morpion"
  puts "(2) Modifier le compte à rebours"
  puts "(3) Modifier le temps d'entrer une reponse"
  puts "(4) Modifier le niveau de l'ordinateur"
  print "(0) Retourner au menu\n\n>>> "
  saisie = gets.chomp.to_i
  case saisie
  when 1
    puts "Entrez la nouvelle taille du morpion : "
    saisie = gets.chomp.to_i
    $n = saisie if saisie >= 2
    $limite = $n*$n
  when 2
    puts "Entrez le nouveau compte à rebours, en secondes (temps minimal 5 sec) : "
    saisie = gets.chomp.to_i
    $tempsLimitePartie = saisie if saisie >= 5
  when 3
    puts "Entrez le nouveau temps limite de saisi, en secondes (temps minimal 2 sec) : "
    saisie = gets.chomp.to_i
    $tempsLimite = saisie if saisie >= 2
  when 0# Quitter
    return
  end
end

def afficheMorpion(plateau)
  """affiche un morpion de la partie en cours
  les colonnes sont designees par a, b, c et
  les lignes par 1, 2, 3"""
  # system "clear" or system "cls"# efface la console
  abc = '`'
  numLigne = 1
  col = 0
  print " "
  3.times do
    print "  ", (abc = abc.next), " "# a b c
  end

  print "\n "
  for numLigne in 0..$n-1
    print "-" * (($n + 1) + $n * 3), "\n", numLigne + 1# 1 2 3
    plateau[numLigne].each { |symbole| print "| ", (symbole == 'X' ? symbole.red: symbole.blue), " " }
    print "| \n "
    numLigne += 1
  end
  print "-" * (($n + 1) + $n * 3), "\n\n"
end# afficheMorpion

def coordonneesExactes?(lig, col)
  """Verifie si les coordonnees entrees par l'utilisateur
  sont bien conformes : entre 1 et 3 et entre a et c
  col[0] donne le code ascii du caractere"""
  return (lig > 0 && lig <= $n) && (col[0] < (97 + $n) && col[0] >= 97)
end# coordonneesExactes

def bonType?(ligne, colonne)
  """retourne un booleen indiquant si la saisie utilisateur est conforme
  au type de variable attendue (ligne:entier; colonne:caractere"""
  return ligne.respond_to?(:to_i) && colonne.respond_to?(:split)
end# bonType

def saisieUtilisateurCorrecte?(ligne, colonne)
  """retourne un booleen indiquant si l'utilsatuer a bien entre une
  colonne et une ligne, que la colonne et la ligne sont du bon type
  et que les coordonnees sont conformes au tableau 2D"""
  return colonne && ligne && bonType?(ligne, colonne) && coordonneesExactes?(ligne, colonne)
end# saisieUtilisateurCorrecte

def joueTour(j, partie)
  """Le tour d'un joueur :
  - entre les coordonnees du tableau/morpion
  - verifie si les coordonnees sont conformes
  - verifie que la case est bien vide
  - verifie si le joueur a gagne ou si le morpion est rempli"""
  debut = Time.new# debut du chronometre
  if j.humain# si le joueur est un humain
    begin
      print "Entrez les coordonnees (ligne colonne) : "
      str = gets# on recupere l'entre
      if Time.now.sec - debut.sec>=$tempsLimite
        puts "Vous avez mis trop de temps ! ( ⓛ ω ⓛ *)"
        return 0# le jeu continue
      end# if
      str=str.chomp.split("")# la saisie est splite pour devenir un tableau
      ligne = str[0].to_i# convertit un string en integer (cast) "1" => 1
      colonne = str[1]
    end until saisieUtilisateurCorrecte?(ligne, colonne) and partie.caseVide?(ligne, colonne)
  else# Ordinateur
    ligne, colonne = iaRandom(partie)# coup aleatoire
  end# if
  partie.plateau[ligne - 1][colonne[0] - 97] = j.symbole
  partie.nbTours += 1# le joueur a joue son tour
  if partie.victoire?(j.symbole)
    return 1# le joueur a gagne
  elsif partie.nbTours >= $limite# si toutes les cases sont remplies ...
    return 3# Egalite
  end# if
  return 0# le jeu continue
end# joueTour

def iaRandom(partie)
  """genere des coordonnees aleatoires, respectivement
  ligne et colonne"""
  a = 'a'
  begin
    rand(3).times do a = a.next end
    ligne, colonne = 1 + rand(3), a
  end until partie.caseVide?(ligne, colonne)
  return ligne, colonne
end

def ordreJeu
  """L'utilisateur choisit qui joue en premier s'il
  choisit de se battre contre l'ordinateur"""
  system "clear" or system "cls"# efface la console
  puts "JOUEUR VS ORDINATEUR\n\n( ↀДↀ)✧"
  puts "- - - - - - - - - - - - - - - - - - - - - - - -\n"
  puts "L'ordinateur joue en premier (O/o/N/n) ? "
  saisie = gets.chomp
  return saisie == 'O' || saisie == 'o' ? [1, 0] : [0, 1]
end

def initialisePartie(ordinateur)
  return Partie.new, ordreJeu, Time.now if ordinateur
  system "clear" or system "cls"# efface la console
  puts "JOUEUR VS JOUEUR\n\n₍˄·͈༝·͈˄₍˄·͈༝·͈˄( ͒ ु•·̫• ू ͒)˄·͈༝·͈˄₎˄·͈༝·͈˄₎"
  puts "- - - - - - - - - - - - - - - - - - - - - - - -\n"
  return Partie.new, [0, 1], Time.now
end

class String
  # colorization
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def yellow
    colorize(33)
  end

  def blue
    colorize(34)
  end

  def pink
    colorize(35)
  end

  def light_blue
    colorize(36)
  end
end

################################
#   APPEL DE LA METHODE MAIN   #
################################

main